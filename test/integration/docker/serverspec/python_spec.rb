require 'serverspec'
set :backend, :exec

describe package('java'), :if => os[:family] == 'ubuntu' do
  it { should be_installed }
end

describe package('java'), :if => (os[:family] == 'redhat' or os[:family] == 'fedora')  do
  it { should be_installed }
end
